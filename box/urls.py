# coding:utf-8
from django.conf.urls import patterns, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'box.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       # surl(r'^admin/', include(admin.site.urls)),
                       url(r'^api/hello/$', 'main.api.hello'),
                       url(r'^api/getTopBar$', 'main.api.getTopBar'),
                       url(r'^api/getLeftBar$', 'main.api.getLeftBar'),
                       url(r'^order/get_details_by_wave_and_sku$', 'main.api.get_details_by_wave_and_sku'),

                       url(r'^api/get_menu$', 'main.api.get_menu'),
                       url(r'^api/get_data$', 'main.api.get_data'),
                       url(r'^api/get_book_content$', 'main.api.get_book_content'),
                       url(r'^api/get_video_detail', 'main.api.get_video_detail'),
                       url(r'^api/get_qr_code', 'main.api.get_qr_code'),
                       url(r'^api/star', 'main.api.star'),
                       url(r'^api/feedback', 'main.api.feedback'),
                       url(r'^api/get_company_info', 'main.api.get_company_info'),
)
