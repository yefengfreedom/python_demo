# coding:utf-8
from django.http import HttpResponse
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt



# class JSONResponse(HttpResponse):
# def __init__(self, data, **kwargs):
# content = JSONRenderer().render(data)
# kwargs['content_type'] = 'application/json'
# super(JSONResponse, self).__init__(content, **kwargs)


def returnResponse(code=0, msg='', data=''):
    responseJson = dict()
    responseJson['code'] = code
    responseJson['message'] = msg
    responseJson['data'] = data
    return HttpResponse(simplejson.dumps(responseJson), mimetype='application/json')


def hello(request):
    return HttpResponse(
        '{"status":-6,"msg":"登录信息格式有误","xhprof":"http://xhprof.test.dachuwang.com/xhprof_html/index.php?run=557112bd7659e&source=dachuwang"}')


def getTopBar(request):
    r_data = []
    t1 = dict()
    t1['id'] = 1
    t1['type'] = 1
    t1['name'] = "视频"
    r_data.append(t1)
    t2 = dict()
    t2['id'] = 2
    t2['type'] = 2
    t2['name'] = "图书点评推荐"
    r_data.append(t2)
    t3 = dict()
    t3['id'] = 3
    t3['type'] = 1
    t3['name'] = "文集阅览"
    r_data.append(t3)
    t4 = dict()
    t4['id'] = 3
    t4['type'] = 1
    t4['name'] = "精彩视频推荐"
    r_data.append(t4)
    return returnResponse(0, '', r_data)


def getLeftBar(request):
    r_data = []
    t1 = dict()
    t1['id'] = 1
    t1['type'] = 1
    t1['name'] = "正心正举理论"
    r_data.append(t1)
    t2 = dict()
    t2['id'] = 2
    t2['type'] = 1
    t2['name'] = "哲学"
    r_data.append(t2)
    t3 = dict()
    t3['id'] = 3
    t3['type'] = 1
    t3['name'] = "心理学"
    r_data.append(t3)
    return returnResponse(0, '', r_data)


@csrf_exempt
def get_details_by_wave_and_sku(request):
    rs = '{"status":"0","msg":"查询成功","data":[{"id":"62145","order_id":"33774","product_id":"6016","name":"玉兰一级大豆油（5L*4）","quantity":"1","price":125,"sum_price":125,"spec":[{"name":"品牌","id":"2","val":"玉兰"},{"name":"规格","id":"6","val":"5L*4"}],"status":"14","created_time":"2015/05/31 04:02","updated_time":"2015/06/02 17:15","unit_id":"箱","actual_quantity":"0","actual_price":0,"actual_sum_price":0,"sku_number":"1000695","category_id":"11","single_price":125,"close_unit":"箱","city_id":"993","wave_id":"0","pick_task_id":"0","suborder_id":"34108","remarks":"","line":"大厨江桥商圈","deliver_addr":"上海市普陀区金鼎路1830号农贸市场里面","mobile":"15205766876","shop_name":"早点摊","realname":"吴大姐","geo":"","address":"上海市普陀区金鼎路1830号农贸市场里面"},{"id":"68235","order_id":"36067","product_id":"6102","name":"五得利特精高筋小麦粉（山东菏泽东明）","quantity":"6","price":93,"sum_price":558,"spec":[{"name":"品牌","id":"2","val":"五得利"},{"name":"规格","id":"6","val":"25kg*1"}],"status":"14","created_time":"2015/06/02 01:47","updated_time":"2015/06/02 17:15","unit_id":"袋","actual_quantity":"0","actual_price":0,"actual_sum_price":0,"sku_number":"1000016","category_id":"178","single_price":93,"close_unit":"袋","city_id":"993","wave_id":"0","pick_task_id":"0","suborder_id":"36401","remarks":"公司能不能上点同丰（纯）大豆油","line":"大厨真如商圈","deliver_addr":"上海市普陀区石泉路街道岚皋路175号","mobile":"18501795679","shop_name":"三味拉面馆","realname":"谢老板","geo":"","address":"上海市普陀区石泉路街道岚皋路175号"}]}'
    return HttpResponse(rs)


@csrf_exempt
def get_menu(request):
    rs = '{"left_bar":[{"type":1,"id":1,"name":"正心正举理念"},{"type":1,"id":2,"name":"哲学"},{"type":1,"id":3,"name":"心理学"},{"type":1,"id":4,"name":"教育学"},{"type":1,"id":5,"name":"语言沟通学"},{"type":1,"id":6,"name":"人类学"},{"type":1,"id":7,"name":"科技创新"},{"type":1,"id":8,"name":"人工智能"},{"type":1,"id":9,"name":"跨界思维论坛"},{"type":1,"id":10,"name":"案例分析"},{"type":1,"id":11,"name":"助力公益"}],"top_bar":[{"type":1,"id":1,"name":"视频"},{"type":2,"id":2,"name":"图书点评推荐"},{"type":1,"id":3,"name":"文集阅览"},{"type":1,"id":4,"name":"精彩视频推荐"},{"type":1,"id":5,"name":"播放记录"},{"type":1,"id":6,"name":"我的收藏"}]}'
    return returnResponse(0, '', rs)


@csrf_exempt
def get_data(request):
    rs = '[{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":2,"type":2,"name":"图书点评推荐","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":3,"type":3,"name":"文集阅览","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":4,"type":1,"name":"精彩视频推荐","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":5,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":6,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":7,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":8,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":9,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":10,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":11,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":12,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":13,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"}]'
    return returnResponse(0, '', rs)


@csrf_exempt
def get_book_content(request):
    rs = '这是书籍内容'
    return returnResponse(0, '', rs)


@csrf_exempt
def get_video_detail(request):
    rs = '{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍","kcgy":"这是课程内容","ljswdt":"这是逻辑思维导图介绍","ljswdt_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","kcnr":[{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"}],"xgkc":[{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"},{"id":1,"type":1,"name":"思想力交流视频","img_url":"http://d.hiphotos.baidu.com/image/pic/item/1e30e924b899a9015fbc663b1f950a7b0208f535.jpg","vedio_id":"55DF1D586481F9969C33DC5901307461","des":"这是介绍"}]}'
    return returnResponse(0, '', rs)


@csrf_exempt
def get_qr_code(reqeust):
    rs = 'http://a.hiphotos.baidu.com/baike/g%3D0%3Bw%3D268/sign=7bcb659c9745d688b302b7afd3ff4f28/2934349b033b5bb571dc8c5133d3d539b600bc12.jpg';
    return returnResponse(0, '', rs)


@csrf_exempt
def star(request):
    rs = ''
    return returnResponse(0, '', rs)


@csrf_exempt
def feedback(request):
    return returnResponse(0, '', '')

@csrf_exempt
def get_company_info(request):
    rs='{"name":"北京正心正举国际文化传媒有限公司","idea":"内观己心    外察世界    启迪多元思维\n正心正举    拔迷见智    开启自在之门","logo":"https://ss0.baidu.com/7Po3dSag_xI4khGko9WTAnF6hhy/image/h%3D360/sign=79293a62955298221a333fc5e7cb7b3b/77094b36acaf2edd9cc64f4c881001e93901935b.jpg"}'
    return returnResponse(0,'',rs)